package com.example.nit_appdev_002.feedtemplate.Adapters

import android.animation.AnimatorInflater
import android.animation.StateListAnimator
import android.content.Context
import android.net.Uri
import android.os.Build
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.ViewUtils
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.example.nit_appdev_002.feedtemplate.R
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.nit_appdev_002.feedtemplate.APIObjects.Article
import io.realm.RealmRecyclerViewAdapter
import io.realm.RealmResults


/**
 * Created by NIT_APPDEV_002 on 2/5/2018.
 */
class FeedAdapter  : RealmRecyclerViewAdapter<Article, FeedAdapter.MyViewHolder> {


    constructor(feed:RealmResults<Article>?): super(feed,true, true)

    var onItemClick: (Int) -> Unit ={}

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView
        var description: TextView
        var image: ImageView
        var context: Context
        var cardView: CardView
        init {
            title = view.findViewById(R.id.feed_title) as TextView
            description = view.findViewById(R.id.feed_description) as TextView
            image = view.findViewById(R.id.feed_image) as ImageView
            context = view.context
            cardView = view.findViewById(R.id.cardView) as CardView
        }

        fun bind(article: Article){
            // cast the generic view holder to our specific one
            // set the title and the snippet

            val id = article.id
            title.text = article.title
            description.text = article.description

            if (article.imagePath != null){
                Glide.with(context)
                        .load(Uri.parse(article.imagePath))
                        .apply(RequestOptions()
                                .fitCenter()
                                .placeholder(R.drawable.image_placeholder)
                                .dontAnimate())
                        .into(image)
            }
            else{
                image.setImageResource(R.drawable.image_placeholder)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder{
        val itemView:View = LayoutInflater.from(parent.context)
                .inflate(R.layout.feed_card, parent, false)
        val holder = MyViewHolder(itemView)

        /* Animation code from Kwaw*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val stateListAnimator:StateListAnimator  = AnimatorInflater
                    .loadStateListAnimator(parent.context, R.animator.main_feed_elevate_click)
            holder.itemView.setStateListAnimator(stateListAnimator);
        }
        // add a click handler to ensure the CardView handles touch events
        // otherwise the animation won't work
        holder.itemView.setOnClickListener{
            onItemClick(holder.adapterPosition)
        }

        return holder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val article:Article? = getItem(holder.adapterPosition)
        if(article != null){
            holder.bind(article)
        }
    }
}