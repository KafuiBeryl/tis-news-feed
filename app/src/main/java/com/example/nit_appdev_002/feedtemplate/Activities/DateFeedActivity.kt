package com.example.nit_appdev_002.feedtemplate.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.nit_appdev_002.feedtemplate.APIObjects.Articles
import com.example.nit_appdev_002.feedtemplate.Adapters.FeedAdapter
import com.example.nit_appdev_002.feedtemplate.R
import com.example.nit_appdev_002.feedtemplate.Realm.RealmManager
import com.example.nit_appdev_002.feedtemplate.Utils.INTENT_ARTICLE
import com.example.nit_appdev_002.feedtemplate.Utils.INTENT_ARTICLE_DATE
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_date_feed.*
import kotlinx.android.synthetic.main.recycler.*
import java.text.SimpleDateFormat
import java.util.*

class DateFeedActivity : AppCompatActivity() {

    private val realmManager = RealmManager()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date_feed)
        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //handle realm creation and disposal
        realmManager.incrementCount()
        val realm = Realm.getDefaultInstance()

        val date:Long

        if (savedInstanceState == null){
            date = intent.getLongExtra(INTENT_ARTICLE_DATE, 0)
        }
        else{
            date = savedInstanceState.getSerializable(INTENT_ARTICLE_DATE) as Long
        }

        val list = realm.where(Articles::class.java).equalTo("date",date ).findFirst()


        recycle_view.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        recycle_view.layoutManager = layoutManager
        recycle_view.recycledViewPool.setMaxRecycledViews(0,0)
        val adapter = FeedAdapter(list?.articles?.where()?.findAll())

        adapter.onItemClick = {position ->
            val item = adapter.getItem(position)?.article
            if (item != null){
                val intent = Intent(this, ArticleActivity::class.java)
                intent.putExtra(INTENT_ARTICLE, item)
                this.startActivity(intent)
            }
        }
        recycle_view.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        realmManager.decrementCount()
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent != null){
            setIntent(intent)
        }
    }
}
