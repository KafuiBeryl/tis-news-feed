package com.example.nit_appdev_002.feedtemplate.Utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by NIT_APPDEV_002 on 3/14/2018.
 */
class HelperWithoutContext {
    fun getDateTime(s: Long?): String? {
        try {
            val sdf = SimpleDateFormat("EEE MMM dd yyyy")
            val netDate = Date(s!!*1000)
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }
}