package com.example.nit_appdev_002.feedtemplate.Activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.nit_appdev_002.feedtemplate.R
import com.example.nit_appdev_002.feedtemplate.Utils.INTENT_ARTICLE

import kotlinx.android.synthetic.main.activity_article.*
import kotlinx.android.synthetic.main.content_article.*

class ArticleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_article)
        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        var artString:String
        if (savedInstanceState == null){
            artString = intent.getStringExtra(INTENT_ARTICLE)
            if (artString != null){
                art_webView.loadData(artString, "text/html; charset=utf-8", "utf-8")
            }
        }
        else{
            artString = savedInstanceState.getSerializable(INTENT_ARTICLE) as String
            art_webView.loadData(artString, "text/html; charset=utf-8", "utf-8")
        }
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent != null){
            setIntent(intent)
        }
    }
}
