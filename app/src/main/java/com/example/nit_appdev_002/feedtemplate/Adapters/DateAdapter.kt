package com.example.nit_appdev_002.feedtemplate.Adapters

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import com.example.nit_appdev_002.feedtemplate.APIObjects.ArticleDate
import io.realm.RealmRecyclerViewAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.nit_appdev_002.feedtemplate.R
import com.example.nit_appdev_002.feedtemplate.Utils.HelperWithoutContext
import io.realm.RealmResults
import java.text.DateFormat

/**
 * Created by NIT_APPDEV_002 on 2/8/2018.
 */
class DateAdapter : RealmRecyclerViewAdapter<ArticleDate, DateAdapter.OurViewHolder> {

    constructor(feed: RealmResults<ArticleDate>?): super(feed,true, true)

    var onItemClick: (Int) -> Unit ={}

    inner class OurViewHolder(view:View) : RecyclerView.ViewHolder(view){
        var date : TextView
//        var icon : ImageView
        var context : Context
        var cardView : CardView


        init {
            date = view.findViewById(R.id.date_text) as TextView
//            icon = view.findViewById(R.id.icon) as ImageView
            context = view.context
            cardView = view.findViewById(R.id.card) as CardView

//            itemView.setOnClickListener(View.OnClickListener {
//
//            })
        }

        fun bind(dates:ArticleDate){
            val helper = HelperWithoutContext()
            date.text = helper.getDateTime(dates.articleDate)
//            icon.setImageResource(R.drawable.ic_chevron_right_black_24dp)

//            cardView.setOnClickListener{
//                val intent = Intent(context, DateFeedActivity::class.java)
//                intent.putExtra(INTENT_ARTICLE_DATE, dates.articleDate)
//                context.startActivity(intent)
//            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OurViewHolder{
        val view:View = LayoutInflater.from(parent.context)
                .inflate(R.layout.date_card, parent, false)
        val holder = OurViewHolder(view)
        holder.itemView.setOnClickListener{
            onItemClick(holder.adapterPosition)
        }

        return holder
    }

    override fun onBindViewHolder(holder: OurViewHolder, position: Int) {
        val dates:ArticleDate? = getItem(holder.adapterPosition)
        if(dates != null){
            holder.bind(dates)
        }
    }

//    fun setClickListener(callback:View.OnClickListener){
//        val mClickListener = callback
//    }
}