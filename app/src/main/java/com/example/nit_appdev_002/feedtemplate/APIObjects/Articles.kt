package com.example.nit_appdev_002.feedtemplate.APIObjects

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

/**
 * Created by NIT_APPDEV_002 on 1/22/2018.
 */
@RealmClass
open class Article (
        var id: Int = -1,
        var title:String = "",
        var description:String? = "",
        var article:String = "",
        var rank:Int = -1,
        @SerializedName("image_path")
        var imagePath:String? = "",
        @SerializedName("article_date")
        var articleDate:String? = null,
        @SerializedName("created_at")
        var createdAt:String? =  null,
        @SerializedName("updated_at")
        var updatedAt:String? = null
):RealmModel

@RealmClass
open class Articles(
        @PrimaryKey
        var id: Int? = -1,
        @Index
        var date:Long? = null,
        var articles:RealmList<Article>? = null
):RealmModel

@RealmClass
open class ArticleDate(
        @PrimaryKey
        var id: Int = -1,
        @Index
        @SerializedName("article_date")
        var articleDate: Long? = null
): RealmModel

@RealmClass
open class ArticleByDate(
//        @PrimaryKey
//        var id: Int? = -1,
        @Index
        var date:Long? = null,
        var articles:RealmList<Article>? = null
): RealmModel