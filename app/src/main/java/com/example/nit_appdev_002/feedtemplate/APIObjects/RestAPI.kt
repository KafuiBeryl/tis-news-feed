package com.example.nit_appdev_002.feedtemplate.APIObjects

import com.example.nit_appdev_002.feedtemplate.Utils.BASEURL
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.sql.Date
import java.time.format.DateTimeFormatter

/**
 * Created by NIT_APPDEV_002 on 1/23/2018.
 */
class RestAPI{
    private val feedApiService : FeedApiService

//    val realm = Realm.getDefaultInstance()

    init {
//        val moshi = Moshi.Builder()
//                .add(KotlinJsonAdapterFactory())
//                .add(RealmListJsonAdapterFactory())
//                .build()
//        val gsonBuilder:Gson = GsonBuilder()
//                .registerTypeAdapter(Date::class.java, DateDeserializer())
//                .create()

       val retrofit = Retrofit.Builder()
               .baseUrl(BASEURL)
               .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
               .addConverterFactory(GsonConverterFactory.create())
               .build()

        feedApiService = retrofit.create(FeedApiService::class.java)
    }

    fun getArticles(): Call<List<Articles>>{
        return feedApiService.getArticles()
    }

    fun getArticleDates(): Call<List<ArticleDate>> {
        return feedApiService.getArticleDates()
    }

    fun getSingleArticle(date: Date) : Call<ArticleByDate>{
        return feedApiService.getArticleByDate(date)
    }
}

//class ArticlesAdapter:JsonAdapter<Articles>(){
//
//
//    @ToJson override fun fromJson(jsonReader: JsonReader): Articles?{
//        val value = jsonReader.
//    }
//}

