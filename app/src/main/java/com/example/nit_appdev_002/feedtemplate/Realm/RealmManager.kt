package com.example.nit_appdev_002.feedtemplate.Realm

import android.util.Log

import io.realm.Realm
import io.realm.RealmConfiguration
/**
 * Created by NIT_APPDEV_002 on 2/6/2018.
 */
class RealmManager {
    private val TAG = "RealmManager"

    private var realm: Realm? = null

    private var realmConfiguration: RealmConfiguration? = null

    fun initializeRealmConfig() {
        if (realmConfiguration == null) {
            Log.d(TAG, "Initializing Realm configuration.")
            setRealmConfiguration(RealmConfiguration.Builder() //
                    //.initialData(RealmInitialData())
                    .deleteRealmIfMigrationNeeded()
                    .build())
        }
    }

    fun setRealmConfiguration(realmConfig: RealmConfiguration) {
        realmConfiguration = realmConfig
        Realm.setDefaultConfiguration(realmConfig)
    }

    private var activityCount = 0

    fun getRealm(): Realm? {
        return realm
    }

    fun incrementCount() {
        if (activityCount == 0) {
            if (realm != null) {
                if (!realm!!.isClosed) {
                    Log.w(TAG, "Unexpected open Realm found.")
                    realm!!.close()
                }
            }
            Log.d(TAG, "Incrementing Activity Count [0]: opening Realm.")
            realm = Realm.getDefaultInstance()
        }
        activityCount++
        Log.d(TAG, "Increment: Count [$activityCount]")
    }

    fun decrementCount() {
        activityCount--
        Log.d(TAG, "Decrement: Count [$activityCount]")
        if (activityCount <= 0) {
            Log.d(TAG, "Decrementing Activity Count: closing Realm.")
            activityCount = 0
            realm!!.close()
            if (Realm.compactRealm(Realm.getDefaultConfiguration()!!)) {
                Log.d(TAG, "Realm compacted successfully.")
            }
            realm = null
        }
    }
}