package com.example.nit_appdev_002.feedtemplate.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.nit_appdev_002.feedtemplate.Utils.PREF_AGREE
import com.example.nit_appdev_002.feedtemplate.Utils.PreferenceHelper
import kotlinx.android.synthetic.main.activity_terms.*
import com.example.nit_appdev_002.feedtemplate.Utils.PreferenceHelper.set
import com.example.nit_appdev_002.feedtemplate.R

class TermsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //get default prefs
        val prefs = PreferenceHelper.defaultPrefs(this)

        setContentView(R.layout.activity_terms)

        agree_btn.isEnabled = false

        checkboxAgree.setOnCheckedChangeListener{ view, isChecked ->
            agree_btn.isEnabled = true

            if(!isChecked){
                agree_btn.isEnabled = false
            }
        }

        agree_btn.setOnClickListener {
            //set any type of value in prefs
            prefs[PREF_AGREE] = true
            startActivity(Intent(this, ViewFeedActivity:: class.java))
            finish()
        }

    }


}
