package com.example.nit_appdev_002.feedtemplate.Fragments

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.nit_appdev_002.feedtemplate.APIObjects.ArticleDate
import com.example.nit_appdev_002.feedtemplate.APIObjects.Articles
import com.example.nit_appdev_002.feedtemplate.APIObjects.FeedManager
import com.example.nit_appdev_002.feedtemplate.APIObjects.RestAPI
import com.example.nit_appdev_002.feedtemplate.Activities.ArticleActivity
import com.example.nit_appdev_002.feedtemplate.Activities.ViewFeedActivity
import com.example.nit_appdev_002.feedtemplate.Adapters.FeedAdapter
import com.example.nit_appdev_002.feedtemplate.R
import com.example.nit_appdev_002.feedtemplate.Realm.RealmDataHelper
import com.example.nit_appdev_002.feedtemplate.Realm.RealmManager
import com.example.nit_appdev_002.feedtemplate.Utils.GeneralHelper
import com.example.nit_appdev_002.feedtemplate.Utils.INTENT_ARTICLE
import com.example.nit_appdev_002.feedtemplate.Utils.isDownloading
import io.realm.Realm
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_latest_feed.*
import kotlinx.coroutines.experimental.async

/**
 * Created by NIT_APPDEV_002 on 2/2/2018.
 */
class LatestFeedFragment : Fragment(){


    private val realmManager = RealmManager()

    private val realm = Realm.getDefaultInstance()

    private val adapter = FeedAdapter(realm.where(Articles::class.java).sort("date", Sort.DESCENDING).findFirst()?.articles?.where()?.sort("rank", Sort.DESCENDING)?.findAll())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        realmManager.incrementCount()


        val view = inflater.inflate(R.layout.fragment_latest_feed,container,false)

        val list = realm.where(Articles::class.java).sort("date", Sort.ASCENDING).findFirst()

        if(!isDownloading && list == null){
            Toast.makeText(this.context,R.string.feed_toast,Toast.LENGTH_LONG).show()

        }

        else if (isDownloading && list == null){
            Toast.makeText(this.context,R.string.feed_toast,Toast.LENGTH_LONG).show()
        }

        val recycle_view = view.findViewById<RecyclerView>(R.id.recycle_view)
        recycle_view.setHasFixedSize(true)
//        recycle_view.recycledViewPool.setMaxRecycledViews(0,0)

//        val adapter  = adapter

        adapter.onItemClick = {position ->
            val item = adapter.getItem(position)?.article
            if (item != null){
                val intent = Intent(activity, ArticleActivity::class.java)
                intent.putExtra(INTENT_ARTICLE, item)
                activity?.startActivity(intent)
            }
        }

        recycle_view.adapter = adapter
        recycle_view.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)

        Log.e("LATEST_FRAGMENT", "Run this town")
        return view
    }

    override fun onResume() {
        super.onResume()
        refreshData()
    }

    fun refreshData(){
        adapter.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        realmManager.decrementCount()
    }


}





