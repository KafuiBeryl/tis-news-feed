package com.example.nit_appdev_002.feedtemplate.Fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.nit_appdev_002.feedtemplate.APIObjects.ArticleDate
import com.example.nit_appdev_002.feedtemplate.APIObjects.FeedManager
import com.example.nit_appdev_002.feedtemplate.Activities.DateFeedActivity
import com.example.nit_appdev_002.feedtemplate.Adapters.DateAdapter
import com.example.nit_appdev_002.feedtemplate.R
import com.example.nit_appdev_002.feedtemplate.Realm.RealmManager
import com.example.nit_appdev_002.feedtemplate.Utils.GeneralHelper
import com.example.nit_appdev_002.feedtemplate.Utils.INTENT_ARTICLE_DATE
import com.example.nit_appdev_002.feedtemplate.Utils.LayoutItemDecorator
import com.example.nit_appdev_002.feedtemplate.Utils.isDownloading
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_archive_feed.*
import kotlinx.coroutines.experimental.async

/**
 * Created by NIT_APPDEV_002 on 2/2/2018.
 */
class ArchiveFeedFragment : Fragment() {

    private val realmManager = RealmManager()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        getDates()
        realmManager.incrementCount()
        return inflater.inflate(R.layout.fragment_archive_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val realm = Realm.getDefaultInstance()
        recycler_view.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
//        recycler_view.recycledViewPool.setMaxRecycledViews(0,0)
        recycler_view.layoutManager = layoutManager

        val list = realm.where(ArticleDate::class.java).findAll()

        if(!isDownloading && list == null){
            Toast.makeText(this.context,R.string.feed_toast,Toast.LENGTH_LONG).show()
//            getDates()
        }

        else if (isDownloading && list == null){
            Toast.makeText(this.context,R.string.feed_toast,Toast.LENGTH_LONG).show()
        }

//        recycler_view.addItemDecoration(LayoutItemDecorator(this.context!!))

        val mAdapter = DateAdapter(realm.where(ArticleDate::class.java).findAll())

        mAdapter.onItemClick = {position ->
          val item = mAdapter.getItem(position)?.articleDate
            if (item != null){
                val intent = Intent(context, DateFeedActivity::class.java)
                intent.putExtra(INTENT_ARTICLE_DATE, item.toLong())
                activity?.startActivity(intent)
            }
        }

        recycler_view.adapter = mAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        realmManager.decrementCount()
    }
}