package com.example.nit_appdev_002.feedtemplate.Activities

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils.replace
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.nit_appdev_002.feedtemplate.APIObjects.ArticleDate
import com.example.nit_appdev_002.feedtemplate.APIObjects.Articles
import com.example.nit_appdev_002.feedtemplate.APIObjects.RestAPI
import com.example.nit_appdev_002.feedtemplate.Fragments.ArchiveFeedFragment
import com.example.nit_appdev_002.feedtemplate.Fragments.LatestFeedFragment
import com.example.nit_appdev_002.feedtemplate.R
import com.example.nit_appdev_002.feedtemplate.Realm.RealmDataHelper
import com.example.nit_appdev_002.feedtemplate.Utils.GeneralHelper
import com.example.nit_appdev_002.feedtemplate.Utils.TAG_ARCHIVE
import com.example.nit_appdev_002.feedtemplate.Utils.TAG_LATEST
import com.example.nit_appdev_002.feedtemplate.Utils.isDownloading
import kotlinx.android.synthetic.main.activity_view_feed.*
import kotlinx.android.synthetic.main.app_bar_view_feed.*
import java.io.IOException

class ViewFeedActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_feed)
        setSupportActionBar(toolbar)

        val helper = GeneralHelper(this) // check connection

        if (helper.isConnected()){
            try {
                val downTaskTwo = SecondDownloadTask()
                downTaskTwo.execute()
            }
            catch(e:Exception){
                Log.e("ViewFeed_AsyncTask", e.message)
            }
        }else{
            Toast.makeText(this,R.string.check_internet,Toast.LENGTH_LONG).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onStart() {
        super.onStart()
        initialLoadFragment()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.view_feed, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        loadFragment(id)

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun fragmentTransaction(fragment:Fragment, tag:String){
        supportFragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment, tag)
                .commit()
        supportFragmentManager.executePendingTransactions()
    }

    private fun loadFragment(menuId:Int){
        var fragment: Fragment
        var tag:String
        when(menuId){
            R.id.nav_latest -> {
                fragment = LatestFeedFragment()
                tag = TAG_LATEST
                fragmentTransaction(fragment, tag)
            }
            R.id.nav_archive -> {
                fragment = ArchiveFeedFragment()
                tag = TAG_ARCHIVE
                fragmentTransaction(fragment, tag)
            }
        }

    }

    private fun initialLoadFragment(){
        val fragment = LatestFeedFragment()
        var tag = TAG_LATEST
        fragmentTransaction(fragment, tag)
    }

    fun refreshFragment(){
//        supportFragmentManager.executePendingTransactions()

        if (!isFinishing && !isDestroyed){
//            supportFragmentManager.beginTransaction()
//                    .replace(R.id.frame_container, LatestFeedFragment(), tag)
//                    .commit()
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, LatestFeedFragment(), TAG_LATEST)
                    .commitAllowingStateLoss()
//            fragmentManager.executePendingTransactions()
//            val count: Int = fragmentManager.backStackEntryCount -1
//            val tag = fragmentManager.getBackStackEntryAt( 0).name
//            val currentFragment:Fragment = fragmentManager.findFragmentByTag(tag) as Fragment
//            val fragmentTransaction = fragmentManager.beginTransaction()
//            fragmentTransaction.detach(currentFragment)
//            fragmentTransaction.attach(LatestFeedFragment())
//            fragmentTransaction.commit()

        }
    }

    override fun onPostResume() {
        super.onPostResume()
//        if (!isFinishing && !isDestroyed){
//            supportFragmentManager.beginTransaction()
//                    .replace(R.id.frame_container, LatestFeedFragment(), tag)
//                    .commit()

//                        val fragmentManager = supportFragmentManager
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.frame_container, LatestFeedFragment(), TAG_LATEST)
//                                .commit()
//                        fragmentManager.executePendingTransactions()

//            val count: Int = fragmentManager.backStackEntryCount -1
//            val tag = fragmentManager.getBackStackEntryAt( 0).name
//            val currentFragment:Fragment = fragmentManager.findFragmentByTag(tag) as Fragment
//            val fragmentTransaction = fragmentManager.beginTransaction()
//            fragmentTransaction.detach(currentFragment)
//            fragmentTransaction.attach(LatestFeedFragment())
//            fragmentTransaction.commit()

//        }
    }

    class DownloadTask : AsyncTask<Void, Void, List<Articles>>() {
        override fun doInBackground(vararg params: Void?): List<Articles>? {
            try{
                val api: RestAPI = RestAPI()
                isDownloading = true
                val callResponse = api.getArticles()
                val response = callResponse.execute()
                if(response.isSuccessful && response.body() != null){
                    @Suppress("UNCHECKED_CAST")
                    val news = response.body() as List<Articles>
                    return news
                }
            } catch (e:IOException){
                Log.e("DownloadTask", e.message)
            }
            return emptyList()

        }

        override fun onPostExecute(result: List<Articles>?) {
            super.onPostExecute(result)
            isDownloading = false

            if (!result!!.isEmpty()){
                val realmDataHelper = RealmDataHelper()
                realmDataHelper.setLatestArticles(result)
            }

            //val activity = ViewFeedActivity()
            //activity.refreshFragment()
        }
    }

    class SecondDownloadTask : AsyncTask<Void, Void, List<ArticleDate>>(){
        override fun doInBackground(vararg p0: Void?): List<ArticleDate>? {
            try {
                val api: RestAPI = RestAPI()
                val callRequest = api.getArticleDates()
                val request = callRequest.execute()
                if (request.isSuccessful) {
                    @Suppress("UNCHECKED_CAST")
                    val news = request.body() as List<ArticleDate>
                    return news
                }
            } catch (e:IOException){
                Log.e("SecondDownloadTask", e.message)
            }
            return emptyList()
        }

        override fun onPostExecute(result: List<ArticleDate>?) {
            super.onPostExecute(result)
            val realmDataHelper = RealmDataHelper()
            if (!result!!.isEmpty()){
                realmDataHelper.setDates(result)
            }

            DownloadTask().execute()
        }
    }

}








