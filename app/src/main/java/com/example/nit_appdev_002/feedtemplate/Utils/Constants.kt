package com.example.nit_appdev_002.feedtemplate.Utils

import android.support.annotation.StringDef

//shared preferences
const val PREF_AGREE = "Agreed"

@Volatile var isDownloading:Boolean = false

//intent extras
const val INTENT_ARTICLE = "Article_Intent"

const val INTENT_ARTICLE_DATE = "Date_Intent"

//API Calls
//const val BASEURL = "http://10.0.2.2:8000/api/"

const val BASEURL = "http://newsapi.rate233.com/api/"

const val API_GETARTICLES = "getArticles"

const val API_ARTICLESBYDATE = "getArticleDates"

const val API_SINGLEARTICLE = "getArticlesByDate/{date}"

//fragment tags
const val TAG_LATEST = "TAG_LATEST"

const val TAG_ARCHIVE = "TAG_ARCHIVE"

