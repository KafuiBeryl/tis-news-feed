package com.example.nit_appdev_002.feedtemplate.Utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress

/**
 * Created by NIT_APPDEV_002 on 2/7/2018.
 */
class GeneralHelper {
    private val contxt:Context

    constructor(context:Context){
        this.contxt = context
    }

    @Throws(InterruptedException::class, IOException::class)
    fun isConnected():Boolean {
//        check if theres internet access
        // if not fuck off
        try {

            doAsync {
                val timeout = 1500
                val sock = Socket()
                val sockAddr:SocketAddress = InetSocketAddress("8.8.8.8",53)
                sock.connect(sockAddr,timeout)
                sock.close()

            }
            return true
        }catch (e:IOException){
            return false
        }
    }
}