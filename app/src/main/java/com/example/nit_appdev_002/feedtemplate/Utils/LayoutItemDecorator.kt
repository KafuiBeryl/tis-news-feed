package com.example.nit_appdev_002.feedtemplate.Utils

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.nit_appdev_002.feedtemplate.R

class LayoutItemDecorator(val context:Context): RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        if (outRect != null && parent != null) {
            val totalWidth = parent.width
            val maxCardWidth = context.resources.getDimensionPixelOffset(R.dimen.card_max_width)
            var sidePdding = (totalWidth - maxCardWidth)/2
            sidePdding = Math.max(0, sidePdding)
            outRect.set(0,sidePdding,0,sidePdding)
        }

    }
}