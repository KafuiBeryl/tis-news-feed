package com.example.nit_appdev_002.feedtemplate.APIObjects

import com.example.nit_appdev_002.feedtemplate.Utils.API_ARTICLESBYDATE
import com.example.nit_appdev_002.feedtemplate.Utils.API_GETARTICLES
import com.example.nit_appdev_002.feedtemplate.Utils.API_SINGLEARTICLE
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import java.sql.Date
import java.time.format.DateTimeFormatter

/**
 * Created by NIT_APPDEV_002 on 1/23/2018.
 */
interface FeedApiService{

    //get all articles
    @GET(API_GETARTICLES)
    fun getArticles(): Call<List<Articles>>

    //get single article with date as parameter
    @GET(API_SINGLEARTICLE)
    fun getArticleByDate(@Path("date") date: Date): Call<ArticleByDate>

    //get all article dates
    @GET(API_ARTICLESBYDATE)
    fun getArticleDates(): Call<List<ArticleDate>>

}