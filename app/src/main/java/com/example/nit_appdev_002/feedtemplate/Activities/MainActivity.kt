package com.example.nit_appdev_002.feedtemplate.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import com.example.nit_appdev_002.feedtemplate.Realm.RealmManager
import com.example.nit_appdev_002.feedtemplate.Utils.PREF_AGREE
import com.example.nit_appdev_002.feedtemplate.Utils.PreferenceHelper
import com.example.nit_appdev_002.feedtemplate.Utils.PreferenceHelper.get
import com.example.nit_appdev_002.feedtemplate.Utils.isDownloading
import io.realm.Realm


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
//        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)

        //get default prefs
//        val prefs = PreferenceHelper.defaultPrefs(this)
//
//        //get any value from prefs
//        val agree: Boolean? = prefs[PREF_AGREE]
//
//        if (agree == true){
//            startActivity(Intent(this, PressReleaseActivity :: class.java))
//            finish()
//        }
//
//        else{
//            startActivity(Intent(this, TermsActivity::class.java))
//            finish()
//        }


//        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()

        val prefs = PreferenceHelper.defaultPrefs(this)

        //get any value from prefs
        val agree: Boolean? = prefs[PREF_AGREE]

        if (agree == true){
            isDownloading = false
            startActivity(Intent(this, ViewFeedActivity:: class.java))
            finish()
        }

        else{
            startActivity(Intent(this, TermsActivity::class.java))
            finish()
        }
    }
}
