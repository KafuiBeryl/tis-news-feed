package com.example.nit_appdev_002.feedtemplate.Utils

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.example.nit_appdev_002.feedtemplate.APIObjects.FeedManager
import com.example.nit_appdev_002.feedtemplate.Realm.RealmManager
import io.realm.Realm
import org.jetbrains.anko.doAsync

/**
 * Created by NIT_APPDEV_002 on 1/24/2018.
 */
class MyApplication : MultiDexApplication(){

    private val context:Context = this

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        doAsync {

            val realmManager = RealmManager()
            realmManager.initializeRealmConfig()
        }
//        callFeed()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

//    fun getAppContext():Context{
//        return context
//    }
//
//    private fun callFeed(){
//        val feedManager = FeedManager()
//        val helper = GeneralHelper()
//        if (helper.isConnected()){
//            feedManager.getFeed()
//        }
//    }
}