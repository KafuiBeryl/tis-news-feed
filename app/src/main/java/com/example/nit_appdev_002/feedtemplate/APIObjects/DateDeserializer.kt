package com.example.nit_appdev_002.feedtemplate.APIObjects

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by NIT_APPDEV_002 on 3/14/2018.
 */


class DateDeserializer : JsonDeserializer<Date> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Date {
        val date:String = json.asString

        val format:SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        format.timeZone = TimeZone.getTimeZone("GMT")

        try {
            return format.parse(date)
        }catch (exp:ParseException){
            System.err.println(exp)
            return format.parse("")
        }
    }
}