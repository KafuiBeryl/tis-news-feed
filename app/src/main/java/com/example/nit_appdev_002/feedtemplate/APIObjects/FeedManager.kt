package com.example.nit_appdev_002.feedtemplate.APIObjects

import com.example.nit_appdev_002.feedtemplate.Realm.RealmDataHelper
import com.example.nit_appdev_002.feedtemplate.Utils.isDownloading
import com.vicpin.krealmextensions.save
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import java.io.IOException

/**
 * Created by NIT_APPDEV_002 on 1/23/2018.
 */
class FeedManager{

    private val api: RestAPI = RestAPI()

    private val realmDataHelper = RealmDataHelper()


//    fun getFeed(){
//        try {
//            val callResponse = api.getArticles()
////            val doResponse = api.getArticleDates()
//            isDownloading = true
//            doAsync(null) {
//                val response = callResponse.execute()
////                val request = doResponse.execute()
//                onComplete {
//                    if(response.isSuccessful){
//                        @Suppress("UNCHECKED_CAST")
//                        val news = response.body() as List<Articles>
//                        realmDataHelper.setLatestArticles(news)
//                        isDownloading = false
//
//                    }
////                    if (request.isSuccessful){
////                        @Suppress("UNCHECKED_CAST")
////                        val dates = response.body() as List<ArticleDate>
////                        realmDataHelper.setDates(dates)
////
////                    }
//
//
//                }
//            }
//        }
//        catch (e:IOException){
//            e.printStackTrace()
//        }
//    }

    suspend fun getFeed() = launch{
        try {
            //Turn on busy indicator.
            isDownloading = true
            val job = async(coroutineContext){
                //We're on a background thread here.
                //Execute blocking calls, such as retrofit call.execute().body() + caching.
                val callResponse = api.getArticles()
                val response = callResponse.execute()
                if(response.isSuccessful){
                    @Suppress("UNCHECKED_CAST")
                    val news = response.body() as List<Articles>
                    realmDataHelper.setLatestArticles(news)
                }
            }
            val secondJob = async(coroutineContext){
                val callResponse = api.getArticleDates()
                val response = callResponse.execute()
                if(response.isSuccessful){
                    @Suppress("UNCHECKED_CAST")
                    val news = response.body() as List<ArticleDate>
                    realmDataHelper.setDates(news)
                }
            }
            job.await()
            secondJob.await()
            //We're back on the main thread here.
            //Update UI controls such as RecyclerView adapter data.

        }catch (e: Exception){

        }finally {
            //Turn off busy indicator.
            isDownloading = false
        }
    }

    fun getDates(){
        try {
            val callResponse = api.getArticleDates()
            isDownloading = true
            doAsync(null) {
                val response = callResponse.execute()
                onComplete {
                    if(response.isSuccessful){
                        @Suppress("UNCHECKED_CAST")
                        val news = response.body() as List<ArticleDate>
                        realmDataHelper.setDates(news)
                        isDownloading = false
                    }
                }
            }
        }
        catch (e:Exception){
            e.printStackTrace()
        }
    }
}